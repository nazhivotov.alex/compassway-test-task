import { api } from 'config'
import { observable, action, makeAutoObservable } from 'mobx'

import { ISignInFieldsData, ISignUpFieldsData, IUser } from 'models/user'

import { getBase64 } from 'utils/helpers'

const endpoint = 'users/'

class Store {
  constructor() {
    makeAutoObservable(this)
  }

  @observable user: IUser | null = null

  @action
  async setUser(user: IUser | null) {
    this.user = user
  }

  @action
  async registerUser(formFields: ISignUpFieldsData): Promise<void | boolean> {
    const { data, status } = await api.post(endpoint, formFields)

    if (data.id !== undefined && status === 201) {
      this.setUser(data)

      localStorage.setItem('username', formFields.username)
      localStorage.setItem('password', formFields.password)
      localStorage.setItem('email', formFields.email)
      api.defaults.headers.common.Authorization = 'Basic ' + getBase64(`${formFields.username}:${formFields.password}`)

      return true
    } else if (status === 400) {
      if (data.username) throw new Error('A user with this username already exists', { cause: 'custom' })
    } else throw new Error('Something went wrong', { cause: 'custom' })
  }

  @action
  async login(formFields: ISignInFieldsData): Promise<void | boolean> {
    // api.defaults.headers.common.Authorization = 'Basic ' + getBase64(`${formFields.username}:${formFields.password}`)

    const { data, status } = await api.get(endpoint + 'current/', {
      headers: {
        accept: 'application/json',
        Authorization: 'Basic ' + getBase64(`${formFields.username}:${formFields.password}`),
      },
    })

    if (data.id !== undefined && status === 200) {
      this.setUser(data)

      localStorage.setItem('username', formFields.username)
      localStorage.setItem('password', formFields.password)

      return true
    } else if (status === 400 || status === 401) {
      if (data.username || data.detail) throw new Error('Incorrect username or password', { cause: 'custom' })
    } else throw new Error('Something went wrong', { cause: 'custom' })
  }
}

export default new Store()
