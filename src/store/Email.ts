import { message } from 'antd'
import { api } from 'config'
import { action, makeAutoObservable } from 'mobx'

import { IEmail, IEmailSend } from 'models/email'

const endpoint = 'emails/'

class Store {
  constructor() {
    makeAutoObservable(this)
  }

  @action
  async getEmailLists(offset: number = 0): Promise<void | { data: IEmail[]; count: number }> {
    const { data, status } = await api.get(endpoint, {
      params: {
        limit: '20',
        offset,
      },
    })
    if (status === 200) return { data: data.results, count: data.count }
    else if (status >= 400 && status <= 500) throw new Error('Something went wrong')
  }

  @action
  async getEmailById(id: string): Promise<void | IEmail> {
    const { data, status } = await api.get(endpoint + id + '/')
    if (status === 200) return data
    else if (status >= 400 && status <= 500) throw new Error('Something went wrong')
  }

  @action
  async sendEmail(formFields: IEmailSend): Promise<void> {
    const { status } = await api.post(endpoint, formFields)
    if (status === 200) message.success('Email successfully sended')
    else if (status >= 400 && status <= 500) throw new Error('Something went wrong')
  }

  @action
  async deleteEmail(id: number): Promise<void> {
    const { status } = await api.delete(endpoint + id + '/')
    if (status === 200) message.success('Email successfully deleted')
    else if (status >= 400 && status <= 500) throw new Error('Something went wrong')
  }
}

export default new Store()
