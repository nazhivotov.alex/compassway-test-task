import React from 'react'

import { LoginOutlined } from '@ant-design/icons'
import { Button } from 'antd'
import { useStore } from 'store'

import styles from './styles.module.scss'

const Header = () => {
  const { userStore } = useStore()

  const handleLogout = () => {
    localStorage.removeItem('username')
    localStorage.removeItem('password')
    userStore.setUser(null)
  }

  return (
    <div className={styles.header}>
      <div className={styles.userInfo}>
        <span>{userStore.user?.username}</span>
        <span>{userStore.user?.email}</span>
      </div>
      <div className={styles.logout}>
        <Button onClick={handleLogout} icon={<LoginOutlined />}>
          Logout
        </Button>
      </div>
    </div>
  )
}

export default Header
