import { Navigate } from 'react-router-dom'
import Header from 'components/Header'

import { IUser } from 'models/user'

interface IProps {
  user: IUser | null
  redirectPath?: string
  children: JSX.Element
}

const ProtectedRoute = ({ user, redirectPath = '/', children }: IProps) => {
  if (!user) {
    return <Navigate to={redirectPath} />
  }

  return (
    <>
      <Header />
      {children}
    </>
  )
}

export default ProtectedRoute
