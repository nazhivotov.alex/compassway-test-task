export interface IUser {
  id: number
  username: string
  email: string
}

export interface ISignUpFieldsData {
  email: string
  username: string
  password: string
}

export interface ISignInFieldsData {
  username: string
  password: string
}
