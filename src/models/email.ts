export interface IEmail {
  id: number
  sender: number
  recipient: number
  subject: string
  message: string
}
export interface IEmailSend {
  sender: number
  recipient: number
  subject: string
  message: string
}
