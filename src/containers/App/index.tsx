import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { useStore } from 'store'
import { observer } from 'mobx-react-lite'

import ProtectedRoute from 'components/ProtectedRoute'
import EmailList from '../Private/EmailList'
import SignIn from '../Public/SignIn'
import SingUp from '../Public/SignUp'
import EmailCreation from 'containers/Private/EmailCreation'

const App = () => {
  const { userStore } = useStore()

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<SignIn />} />
        <Route path="/register" element={<SingUp />} />
        <Route
          path="/email"
          element={
            <ProtectedRoute user={userStore.user}>
              <EmailList />
            </ProtectedRoute>
          }
        />
        <Route
          path="/email/new"
          element={
            <ProtectedRoute user={userStore.user}>
              <EmailCreation />
            </ProtectedRoute>
          }
        />
        <Route
          path="/email/:id"
          element={
            <ProtectedRoute user={userStore.user}>
              <EmailCreation />
            </ProtectedRoute>
          }
        />
      </Routes>
    </BrowserRouter>
  )
}

export default observer(App)
