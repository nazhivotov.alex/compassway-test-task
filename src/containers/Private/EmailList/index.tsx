import React, { useEffect, useState, useMemo } from 'react'
import { DeleteOutlined, ExclamationCircleOutlined } from '@ant-design/icons'
import { Button, message, Table, TableColumnsType } from 'antd'
import confirm from 'antd/lib/modal/confirm'
import { useNavigate } from 'react-router'
import { useStore } from 'store'

import { IEmail } from 'models/email'

import styles from './styles.module.scss'

const PAGE_LIMIT = 2

const EmailList = () => {
  const { emailStore } = useStore()
  const navigate = useNavigate()
  const [isLoading, setIsLoading] = useState(false)
  const [emailList, setEmailList] = useState<IEmail[]>([])
  const [currentPage, setCurrentPage] = useState(1)
  const [emailsTotalCount, setEmailsTotalCount] = useState(0)

  useEffect(() => {
    getUserEmailList()
  }, [])

  const columns: TableColumnsType<IEmail> = useMemo(() => {
    return [
      {
        title: 'Id',
        dataIndex: 'id',
        key: 'id',
        width: 10,
      },
      {
        title: 'Recipient',
        dataIndex: 'recipient',
        key: 'recipient',
      },
      {
        title: 'Subject',
        dataIndex: 'subject',
        key: 'subject',
      },
      {
        key: 'action',
        width: 10,
        render: (_, record) => (
          <Button
            danger
            type="dashed"
            icon={<DeleteOutlined />}
            onClick={e => {
              e.stopPropagation()
              handleDeleteEmail(record.id)
            }}
          />
        ),
      },
    ]
  }, [])

  const handleDeleteEmail = (id: number) => {
    try {
      confirm({
        title: 'Do you Want to delete this email?',
        icon: <ExclamationCircleOutlined />,
        async onOk() {
          await emailStore.deleteEmail(id)
          await getUserEmailList()
        },
      })
    } catch (e: any) {
      message.error(e.message || 'Something went wrong')
    }
  }

  const getUserEmailList = async (page?: number) => {
    try {
      setIsLoading(true)

      const emailListData = await emailStore.getEmailLists(page)

      if (emailListData) {
        setEmailList(emailListData.data)
        setEmailsTotalCount(emailListData.count)
        if (page !== undefined) setCurrentPage(page + 1)
      }
    } catch (e: any) {
      message.error(e.message || 'Something went wrong')
    } finally {
      setIsLoading(false)
    }
  }

  const onTableRow = (record: IEmail) => ({
    onClick: () => navigate('/email/' + record.id),
  })

  const handleWriteNew = () => {
    navigate('/email/new')
  }

  return (
    <div className={styles.emailList}>
      <div className={styles.action}>
        <Button type="primary" onClick={handleWriteNew}>
          Write new email
        </Button>
      </div>

      <div className={styles.table}>
        <Table
          dataSource={emailList}
          columns={columns}
          loading={isLoading}
          onRow={onTableRow}
          rowClassName={styles.tableRow}
          pagination={{
            current: currentPage,
            total: emailsTotalCount,
            pageSize: PAGE_LIMIT,
            onChange: page => getUserEmailList(page - 1),
          }}
        />
      </div>
    </div>
  )
}

export default EmailList
