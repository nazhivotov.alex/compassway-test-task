import React, { useEffect, useState } from 'react'
import { Button, Form, Input, message, Typography } from 'antd'
import { useNavigate, useParams } from 'react-router'
import { useStore } from 'store'

import { emailRe } from 'utils/consts'

import { IEmailSend } from 'models/email'

import { Editor } from '@tinymce/tinymce-react'
import { useForm } from 'antd/lib/form/Form'
import { MailOutlined, SendOutlined } from '@ant-design/icons'

import styles from './styles.module.scss'

const { Title } = Typography

const EmailCreation = () => {
  const { emailStore, userStore } = useStore()
  const [form] = useForm()
  const [editorValue, setEditorValue] = useState('')
  const navigate = useNavigate()
  const { id } = useParams()

  useEffect(() => {
    if (id) getEmailData(id)
  }, [])

  useEffect(() => {
    if (typeof editorValue !== 'string') return
    form.setFields([
      {
        name: 'message',
        value: '3333',
      },
    ])
  }, [editorValue])

  const getEmailData = async (emailId: string) => {
    const emailData = await emailStore.getEmailById(emailId)
    if (emailData) {
      form.setFieldValue('recipient', emailData.recipient)
      form.setFieldValue('subject', emailData.subject)
      form.setFieldValue('message', emailData.message)
      setEditorValue(emailData.message)
    }
  }

  const onFinish = async (formFields: IEmailSend) => {
    try {
      if (!userStore.user?.id) return

      await emailStore.sendEmail({ ...formFields, sender: userStore.user?.id })
      navigate('/email')
    } catch (e: any) {
      message.error(e.message || 'Something went wrong')
    }
  }

  return (
    <div className={styles.emailCreation}>
      <Title level={3}>
        <MailOutlined /> {id ? 'Message View' : 'New Message'}
      </Title>
      <Form
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
        requiredMark={false}
        labelCol={{ span: 4 }}
        className={styles.form}
        form={form}
        disabled={!!id}
      >
        <Form.Item>
          <Input placeholder="Sender" value={userStore.user?.email} bordered={false} className={styles.formInput} />
        </Form.Item>
        <Form.Item
          name="recipient"
          rules={[
            { required: true, message: 'Please input your recipient!' },
            {
              pattern: emailRe,
              message: 'Email is invalid',
            },
          ]}
        >
          <Input placeholder="Recipient" bordered={false} className={styles.formInput} />
        </Form.Item>

        <Form.Item name="subject" rules={[{ required: true, message: 'Please input your subject!' }]}>
          <Input placeholder="Subject" bordered={false} className={styles.formInput} />
        </Form.Item>

        <Form.Item name="message" rules={[{ required: true, message: 'Please input your message!' }]}>
          <div>
            <Editor
              disabled={!!id}
              value={editorValue}
              onEditorChange={value => setEditorValue(value)}
              apiKey={process.env.REACT_APP_TINY_API_KEY}
              init={{
                height: 500,
                menubar: false,
                plugins: [
                  'advlist autolink lists link image charmap print preview anchor',
                  'searchreplace visualblocks code fullscreen',
                  'insertdatetime media table paste code help wordcount',
                ],
                toolbar:
                  'undo redo | formatselect | ' +
                  'bold italic backcolor | alignleft aligncenter ' +
                  'alignright alignjustify | bullist numlist outdent indent | ' +
                  'removeformat | help',
                content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
                placeholder: 'Message',
              }}
            />
          </div>
        </Form.Item>

        {!id && (
          <Form.Item className={styles.formSubmit}>
            <Button type="primary" htmlType="submit" icon={<SendOutlined />}>
              Submit
            </Button>
          </Form.Item>
        )}
      </Form>
    </div>
  )
}

export default EmailCreation
