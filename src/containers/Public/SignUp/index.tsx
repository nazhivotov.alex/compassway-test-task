import React, { useState } from 'react'
import { Alert, Button, Form, Input, Typography } from 'antd'
import { LockTwoTone, MailTwoTone, UserOutlined } from '@ant-design/icons'
import { Link, useNavigate } from 'react-router-dom'
import { useStore } from 'store'

import { ISignUpFieldsData } from 'models/user'

import { emailRe } from 'utils/consts'

import styles from 'sources/styles/login.module.scss'

const { Title, Text } = Typography

const SignUp = () => {
  const { userStore } = useStore()
  const navigate = useNavigate()

  const [errorMessage, setErrorMessage] = useState('')

  const onFinish = async (fieldsData: ISignUpFieldsData) => {
    try {
      const statusOK = await userStore.registerUser(fieldsData)
      if (statusOK) navigate('/email')
    } catch (error: any) {
      error.cause === 'custom' && setErrorMessage(error.message)
    }
  }

  return (
    <div className={styles.container}>
      <div className={styles.block}>
        <div className={styles.introText}>
          <Title level={2}>Create an account</Title>
          <Text type="secondary">Enter your credentials to create new account</Text>
        </div>
        <div className={styles.errorMessage}>
          {errorMessage && (
            <Alert message={errorMessage} type="warning" showIcon closable onClose={() => setErrorMessage('')} />
          )}
        </div>
        <div className={styles.form}>
          <Form name="basic" initialValues={{ remember: true }} onFinish={onFinish} requiredMark={false}>
            <Form.Item
              name="email"
              rules={[
                { required: true, message: 'Please input your email!' },
                {
                  pattern: emailRe,
                  message: 'Email is invalid',
                },
              ]}
            >
              <Input
                placeholder="Enter your email"
                className={styles.formInput}
                prefix={<MailTwoTone className={styles.inputIcon} />}
              />
            </Form.Item>

            <Form.Item name="username" rules={[{ required: true, message: 'Please input your username!' }]}>
              <Input
                placeholder="Enter your username"
                className={styles.formInput}
                prefix={<UserOutlined className={styles.inputIcon} />}
              />
            </Form.Item>

            <Form.Item name="password" rules={[{ required: true, message: 'Please input your password!' }]}>
              <Input.Password
                className={styles.formInput}
                placeholder="Enter your password"
                prefix={<LockTwoTone className={styles.inputIcon} />}
              />
            </Form.Item>

            <Form.Item
              extra={
                <span className={styles.extraContent}>
                  Already have one? <Link to="/">Sing In</Link>
                </span>
              }
            >
              <Button type="primary" htmlType="submit" className={styles.submitButton}>
                Sign Up
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  )
}

export default SignUp
