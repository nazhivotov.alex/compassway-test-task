import React, { useEffect, useState } from 'react'
import { Alert, Button, Form, Input, Typography } from 'antd'
import { LockTwoTone, UserOutlined } from '@ant-design/icons'
import { Link, useNavigate } from 'react-router-dom'
import { useStore } from 'store'
import { ISignInFieldsData } from 'models/user'

import styles from 'sources/styles/login.module.scss'

const { Title, Text } = Typography

const SignIn = () => {
  const { userStore } = useStore()
  const navigate = useNavigate()

  const [errorMessage, setErrorMessage] = useState('')

  useEffect(() => {
    checkAuth()
  }, [])

  const checkAuth = async () => {
    const username = localStorage.getItem('username')
    const password = localStorage.getItem('password')

    if (username && password) {
      try {
        const statusOK = await userStore.login({ username, password })
        if (statusOK) navigate('/email')
      } catch (error: any) {
        localStorage.removeItem('username')
        localStorage.removeItem('password')
        error.cause === 'custom' && setErrorMessage(error.message)
      }
    }
  }

  const onFinish = async (fieldsData: ISignInFieldsData) => {
    try {
      const statusOK = await userStore.login(fieldsData)
      if (statusOK) navigate('/emails')
    } catch (error: any) {
      error.cause === 'custom' && setErrorMessage(error.message)
    }
  }

  return (
    <div className={styles.container}>
      <div className={styles.block}>
        <div className={styles.introText}>
          <Title level={2}>Welcome Back</Title>
          <Text type="secondary">Enter your credentials to access your account</Text>
        </div>
        <div className={styles.errorMessage}>
          {errorMessage && (
            <Alert message={errorMessage} type="warning" showIcon closable onClose={() => setErrorMessage('')} />
          )}
        </div>
        <div className={styles.form}>
          <Form name="basic" initialValues={{ remember: true }} onFinish={onFinish} requiredMark={false}>
            <Form.Item name="username" rules={[{ required: true, message: 'Please input your username!' }]}>
              <Input
                placeholder="Enter your username"
                className={styles.formInput}
                prefix={<UserOutlined className={styles.inputIcon} />}
              />
            </Form.Item>

            <Form.Item name="password" rules={[{ required: true, message: 'Please input your password!' }]}>
              <Input.Password
                className={styles.formInput}
                placeholder="Enter your password"
                prefix={<LockTwoTone className={styles.inputIcon} />}
              />
            </Form.Item>

            <Form.Item
              extra={
                <span className={styles.extraContent}>
                  Don't have an account? <Link to="/register">Sing Up</Link>
                </span>
              }
            >
              <Button type="primary" htmlType="submit" className={styles.submitButton}>
                Sign In
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  )
}

export default SignIn
