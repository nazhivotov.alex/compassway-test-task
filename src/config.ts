import axios, { AxiosResponse } from 'axios'
import { getBase64 } from 'utils/helpers'
import stores from 'store'

const username = localStorage.getItem('username')
const password = localStorage.getItem('password')

export const api = axios.create({
  baseURL: process.env.REACT_APP_HOST,
  headers: {
    Authorization: username && password ? 'Basic ' + getBase64(`${username}:${password}`) : '',
  },
})

api.interceptors.response.use(
  (response: AxiosResponse) => response,
  // Default axios error type
  (error: any) => {
    if (error?.response?.status === 401) {
      fallback()
    }
    return error
  }
)
api.defaults.validateStatus = () => true

const fallback = () => {
  localStorage.removeItem('username')
  localStorage.removeItem('password')
  stores.userStore.setUser(null)
  window.location.reload()
}
