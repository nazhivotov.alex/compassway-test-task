export const getBase64 = (str: string) => {
  return btoa(unescape(encodeURIComponent(str)))
}
